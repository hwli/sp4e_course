import sys

def compute_series(n):
        res = 0 
        for i in range(1, n+1):
                res += i

        return (res)

def main():
        
    print("Hello World!")
    test = sys.argv[0]
    print(f'Hello {test}')

    if len(sys.argv) < 2:
            print("usage: python hello.py <name>")
            return

    prog_name = sys.argv[0]
    N = int (sys.argv[1])
    res_series = compute_series (N)
    
    print("{0} says : sum of {1} is {2}".format(prog_name, N, res_series))


if __name__ == "__main__":
       main()